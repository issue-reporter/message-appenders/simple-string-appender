package com.gitlab.soshibby.issuereporter.messageappenders.simplestringappender;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.messageappender.MessageAppender;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimpleStringAppender implements MessageAppender {

    private Config configuration;

    @Override
    public void init(String config) {
        configuration = Config.from(config);
        validateConfig(configuration);
    }

    @Override
    public String createMessageFrom(LogStatement logTrigger, List<LogStatement> logStatement) {
        return configuration.getOutput();
    }

    private void validateConfig(Config config) {
        Assert.notNull(config, "Configuration is null.");
        Assert.hasText(config.getOutput(), "Output is null or empty in config.");
    }
}
